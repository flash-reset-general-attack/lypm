package com.ly.common.core.constant;

/**
 * 服务名称
 * 
 * @author ly
 */
public class ServiceNameConstants
{
    /**
     * 认证服务的serviceid
     */
    public static final String AUTH_SERVICE = "ly-auth";

    /**
     * 系统模块的serviceid
     */
    public static final String SYSTEM_SERVICE = "ly-system";

    /**
     * 文件服务的serviceid
     */
    public static final String FILE_SERVICE = "ly-file";
}
