package com.ly.common.core.exception;

/**
 * 演示模式异常
 * 
 * @author ly
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
