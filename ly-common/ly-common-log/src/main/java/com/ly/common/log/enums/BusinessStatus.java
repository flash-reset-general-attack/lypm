package com.ly.common.log.enums;

/**
 * 操作状态
 * 
 * @author ly
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
