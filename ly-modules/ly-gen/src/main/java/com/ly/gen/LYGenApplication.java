package com.ly.gen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.ly.common.security.annotation.EnableCustomConfig;
import com.ly.common.security.annotation.EnableRyFeignClients;
import com.ly.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 代码生成
 * 
 * @author ly
 */
@EnableCustomConfig
@EnableCustomSwagger2   
@EnableRyFeignClients
@SpringBootApplication
public class LYGenApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(LYGenApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  代码生成模块启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                "  ██\n" +
                " ░██  ██   ██        █████\n" +
                " ░██ ░░██ ██        ██░░░██  █████  ███████\n" +
                " ░██  ░░███   █████░██  ░██ ██░░░██░░██░░░██\n" +
                " ░██   ░██   ░░░░░ ░░██████░███████ ░██  ░██\n" +
                " ░██   ██           ░░░░░██░██░░░░  ░██  ░██\n" +
                " ███  ██             █████ ░░██████ ███  ░██\n" +
                "░░░  ░░             ░░░░░   ░░░░░░ ░░░   ░░\n");
    }
}
