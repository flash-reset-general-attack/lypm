package com.ly.job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.ly.common.security.annotation.EnableCustomConfig;
import com.ly.common.security.annotation.EnableRyFeignClients;
import com.ly.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 定时任务
 * 
 * @author ly
 */
@EnableCustomConfig
@EnableCustomSwagger2   
@EnableRyFeignClients
@SpringBootApplication
public class LYJobApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(LYJobApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  定时任务模块启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                "  ██                   ██          ██\n" +
                " ░██  ██   ██         ░░          ░██\n" +
                " ░██ ░░██ ██           ██  ██████ ░██\n" +
                " ░██  ░░███   █████   ░██ ██░░░░██░██████\n" +
                " ░██   ░██   ░░░░░    ░██░██   ░██░██░░░██\n" +
                " ░██   ██           ██░██░██   ░██░██  ░██\n" +
                " ███  ██           ░░███ ░░██████ ░██████\n" +
                "░░░  ░░             ░░░   ░░░░░░  ░░░░░\n");
    }
}
