package com.ly.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.ly.common.security.annotation.EnableCustomConfig;
import com.ly.common.security.annotation.EnableRyFeignClients;
import com.ly.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 系统模块
 * 
 * @author ly
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class LYSystemApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(LYSystemApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  系统模块启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                "  ██                                           ██\n" +
                " ░██  ██   ██                ██   ██          ░██\n" +
                " ░██ ░░██ ██         ██████ ░░██ ██   ██████ ██████  █████  ██████████\n" +
                " ░██  ░░███   █████ ██░░░░   ░░███   ██░░░░ ░░░██░  ██░░░██░░██░░██░░██\n" +
                " ░██   ░██   ░░░░░ ░░█████    ░██   ░░█████   ░██  ░███████ ░██ ░██ ░██\n" +
                " ░██   ██           ░░░░░██   ██     ░░░░░██  ░██  ░██░░░░  ░██ ░██ ░██\n" +
                " ███  ██            ██████   ██      ██████   ░░██ ░░██████ ███ ░██ ░██\n" +
                "░░░  ░░            ░░░░░░   ░░      ░░░░░░     ░░   ░░░░░░ ░░░  ░░  ░░\n");
    }
}
