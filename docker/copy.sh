#!/bin/sh

# 复制项目的文件到对应docker路径，便于一键生成镜像。
usage() {
	echo "Usage: sh copy.sh"
	exit 1
}


# copy sql
echo "begin copy sql "
cp ../sql/ry_20210908.sql ./mysql/db
cp ../sql/ry_config_20220114.sql ./mysql/db

# copy html
echo "begin copy html "
cp -r ../ly-ui/dist/** ./nginx/html/dist


# copy jar
echo "begin copy ly-gateway "
cp ../ly-gateway/target/ly-gateway.jar ./ly/gateway/jar

echo "begin copy ly-auth "
cp ../ly-auth/target/ly-auth.jar ./ly/auth/jar

echo "begin copy ly-visual "
cp ../ly-visual/ly-monitor/target/ly-visual-monitor.jar  ./ly/visual/monitor/jar

echo "begin copy ly-modules-system "
cp ../ly-modules/ly-system/target/ly-modules-system.jar ./ly/modules/system/jar

echo "begin copy ly-modules-file "
cp ../ly-modules/ly-file/target/ly-modules-file.jar ./ly/modules/file/jar

echo "begin copy ly-modules-job "
cp ../ly-modules/ly-job/target/ly-modules-job.jar ./ly/modules/job/jar

echo "begin copy ly-modules-gen "
cp ../ly-modules/ly-gen/target/ly-modules-gen.jar ./ly/modules/gen/jar

