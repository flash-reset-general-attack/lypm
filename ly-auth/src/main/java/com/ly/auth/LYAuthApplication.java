package com.ly.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import com.ly.common.security.annotation.EnableRyFeignClients;

/**
 * 认证授权中心
 * 
 * @author ly
 */
@EnableRyFeignClients
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class LYAuthApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(LYAuthApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  认证授权中心启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                "  ██                                    ██   ██\n" +
                " ░██  ██   ██                          ░██  ░██\n" +
                " ░██ ░░██ ██         ██████   ██   ██ ██████░██\n" +
                " ░██  ░░███   █████ ░░░░░░██ ░██  ░██░░░██░ ░██████\n" +
                " ░██   ░██   ░░░░░   ███████ ░██  ░██  ░██  ░██░░░██\n" +
                " ░██   ██           ██░░░░██ ░██  ░██  ░██  ░██  ░██\n" +
                " ███  ██           ░░████████░░██████  ░░██ ░██  ░██\n" +
                "░░░  ░░             ░░░░░░░░  ░░░░░░    ░░  ░░   ░░\n");
    }
}
