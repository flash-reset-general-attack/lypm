package com.ly.modules.monitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import de.codecentric.boot.admin.server.config.EnableAdminServer;

/**
 * 监控中心
 * 
 * @author ly
 */
@EnableAdminServer
@SpringBootApplication
public class LYMonitorApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(LYMonitorApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  监控中心启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                "  ██                                              ██   ██\n" +
                " ░██  ██   ██                                    ░░   ░██\n" +
                " ░██ ░░██ ██        ██████████   ██████  ███████  ██ ██████  ██████  ██████\n" +
                " ░██  ░░███   █████░░██░░██░░██ ██░░░░██░░██░░░██░██░░░██░  ██░░░░██░░██░░█\n" +
                " ░██   ░██   ░░░░░  ░██ ░██ ░██░██   ░██ ░██  ░██░██  ░██  ░██   ░██ ░██ ░\n" +
                " ░██   ██           ░██ ░██ ░██░██   ░██ ░██  ░██░██  ░██  ░██   ░██ ░██\n" +
                " ███  ██            ███ ░██ ░██░░██████  ███  ░██░██  ░░██ ░░██████ ░███\n" +
                "░░░  ░░            ░░░  ░░  ░░  ░░░░░░  ░░░   ░░ ░░    ░░   ░░░░░░  ░░░\n");
    }
}
